
#include "Arena.h"
#include "PlayerPawnBase.h"
#include "SnakeBase.h"

AArena::AArena()
{
	PrimaryActorTick.bCanEverTick = true;

}

void AArena::BeginPlay()
{
	Super::BeginPlay();
	Arena = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SM_Arena"));
	
}

void AArena::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AArena::Interact(AActor* Interactor, bool bIsHead)
{
	auto SnakeActor = Cast<ASnakeBase>(Interactor);
	if (IsValid(SnakeActor))
	{
		if (SnakeActor->LastMoveDirection != EMovementDirection::LEFT || SnakeActor->LastMoveDirection != EMovementDirection::RIGHT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
		}
		else 
		{
			SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
		}
	}
}