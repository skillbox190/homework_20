
#include "SpeedUp.h"
#include "SnakeBase.h"

ASpeedUp::ASpeedUp()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ASpeedUp::BeginPlay()
{
	Super::BeginPlay();
}

void ASpeedUp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ASpeedUp::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast <ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			float NewSpeed = SpeedModificator;
			Snake->MovementSpeedChange(NewSpeed, BonusDuration);
			this->Destroy();
		}
	}
}