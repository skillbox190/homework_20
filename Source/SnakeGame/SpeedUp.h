
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "SpeedUp.generated.h"

class ASnakeBase;

UCLASS()
class SNAKEGAME_API ASpeedUp : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	ASpeedUp();

	UPROPERTY(EditDefaultsOnly)
		float SpeedModificator;

	UPROPERTY(EditDefaultsOnly)
		float BonusDuration;

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;
};
