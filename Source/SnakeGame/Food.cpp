
#include "Food.h"
#include "SnakeBase.h"

AFood::AFood()
{
	PrimaryActorTick.bCanEverTick = true;
	FoodComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FoodComponent"));
}

void AFood::BeginPlay()
{
	Super::BeginPlay();
}

void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast <ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			this->Destroy();
		}
	}
}

